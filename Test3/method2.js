function search_last(dataset, key) {

    let final_index = 0;

    for (let i = dataset.length - 1; i >= 0; i--) {

        if (dataset[i] === key) {
            final_index = i;
            break;
        }

    }

    return final_index;

}

console.log(search_last([2, 4, 5, 3, 11, 1, 7, 4, 3, 2], 2));