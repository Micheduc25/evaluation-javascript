

function search_last(dataset, key) {
    let temp = [];
    let final_index = 0;

    for (let i = 0; i < dataset.length; i++) {
        if (dataset[i] === key) {
            temp.push(i);
        }
    }

    final_index = temp[temp.length - 1];
    return final_index;
}


console.log(search_last(["a", "b", "c", "a", "d", "a"], "c"));
