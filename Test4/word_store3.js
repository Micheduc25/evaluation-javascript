function word_store() {
    this.myletters = [];
}

word_store.prototype.addLetter = function (letter) {
    this.myletters.push(letter);
}

word_store.prototype.removeLast = function () {
    this.myletters.pop();
}

word_store.prototype.displayWord = function () {
    const temp = this.myletters.join("");
    return temp;
}


var newstore = new word_store();

newstore.addLetter("g");
newstore.addLetter("o");
newstore.addLetter("o");
newstore.addLetter("d");

console.log(newstore.displayWord());