class Word_Store {
    constructor() {
        this.myletters = [];
    }

    addLetter = (letter) => {
        this.myletters.push(letter);
    }

    removeLast = () => {
        this.myletters.pop();
    }

    displayWord = () => {
        return this.myletters.join("");
    }
}


var newstore = new Word_Store();

newstore.addLetter("j");
newstore.addLetter("u");
newstore.addLetter("s");
newstore.addLetter("t");
newstore.addLetter("i");
newstore.addLetter("f");
newstore.addLetter("y");

console.log(newstore.displayWord());