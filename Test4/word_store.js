class Word_Store {
    data_source;

    constructor(data_source) {
        this.data_source = data_source;
    }

    displayWord = () => {
        let temp = this.data_source.join("");
        return temp;
    }
}

var myobj = new Word_Store(["a", "b", "e", "g"]);

console.log(myobj.displayWord());