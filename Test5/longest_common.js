
//this function takes as parameter an object of strings
// and returns the longest length

/**
 * 
 * @param {Array<String>} strings 
 */

//we assume the first string is the longest and we go down checking each string
//if the length of the current is greater than that of the previous we set longest to the length of the current
//an so on and so forth
function longestString(strings) {
    var longest = strings[0];

    for (let i = 1; i < strings.length; i++) {
        if (strings[i].length > strings[i - 1].length) {
            longest = strings[i];
        }
    }

    return longest;
}


//below is a custom function which returns true if an element is found in a collection and false otherwise;
function foundIn(item, collection) {
    found = false;
    for (let i = 0; i < collection.length; i++) {
        if (item === collection[i]) {
            found = true;
            break;
        }
    }

    return found;
}


// console.log(longestString(["hello", "hi", "iammichel"]));

/**
 * 
 * @param {String} string 
 * @param {Number} length 
 */



function getSub(string, length) {

    let mystrings = [];

    for (let i = 0; i < string.length; i++) {

        if (string.substring(i, i + length).length === length) {
            mystrings.push(
                string.substring(i, i + length)
            );
        }

    }

    //we return an array containing all strings
    return mystrings;

}


//this function takes a string and returns all the substrings of length zero to the length of the string. 

function getAllSub(string) {
    let allStrings = [];
    for (let i = 1; i <= string.length; i++) {

        //we call the getSub function which takes a string and lenght and returns all strings of that length
        // allStrings.push(getSub(string, i))
        allStrings = allStrings.concat(getSub(string, i));
    }

    return allStrings;
}

// console.log(getAllSub("abcde"));

//alll the above functions are general

function isCommon(string1, string2) {

    if (string1 === string2) {
        return true;

    }
    else {
        return false;
    }
}

function findLongestCommon(strings) {

    let allOcurrences = [];

    //we start by finding all the substrings of all the strings and assigning them to objects
    for (let i = 0; i < strings.length; i++) {
        allOcurrences[i] = getAllSub(strings[i]);
        //check above later

    }

    let commons = [];

    for (let i = 0; i < allOcurrences.length; i++) {

        for (let j = 0; j < allOcurrences[i].length; j++) {


            let isInAll = false;



            for (let k = 0; k < allOcurrences.length; k++) {

                if (k != i) {
                    if (foundIn(allOcurrences[i][j], allOcurrences[k])) {

                        isInAll = true;
                    }

                    else {
                        isInAll = false;
                        break
                    }
                }
            }

            if (isInAll) {
                commons.push(allOcurrences[i][j]);

            }
        }
    }


    const longest = longestString(commons);

    return longest;

}


console.log(findLongestCommon(
    [
        "mihel",
        "michelamor",
        "micheliaertw"
    ]
));





