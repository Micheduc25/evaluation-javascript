/**
 * 
 * @param {String} sentence 
 * this is the sentence who's words have to be counted
 */

function word_count(sentence) {
    const word_array = sentence.split(" ");

    let final_object = {};

    for (let i = 0; i < word_array.length; i++) {

        if (word_array[i] in final_object) {
            final_object[word_array[i]]++;
        }

        else {
            final_object[word_array[i]] = 1;
        }
    }

    return final_object;
}


//here we test the above function

console.log(word_count("upgrade is the best company upgrade is the best company the best yes the best"));


